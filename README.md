Sur ce site, vous pouvez consulter des informations concernant les monuments et les célébrités des villes, et d'autres informations sur les villes.
Vous pouvez aussi ajouter , modifier ou supprimer ces informations sur ce site. 
Pour celà, il faut faire faire parie du groupe administrateurs. 
Il y a egalement un groupe d'utilisateurs qui peuvent s'identifier mais ne peuvent pas faire de modification.
Pour un test vous pouvez utiliser les identifiants suivants: 

Pour un administrateur:
nom d'utilisateur: admin , mot de passe: 4321

Pour un utilisateur normal:
nom d'utilisateur: user , mot de passe: 4221
