package monuPro.securite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConf extends WebSecurityConfigurerAdapter {
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
 
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
 BCryptPasswordEncoder bcpe=getBCPE();
 auth.inMemoryAuthentication().withUser("admin").password(bcpe.encode("4321")).roles("ADMIN");
 auth.inMemoryAuthentication().withUser("user").password(bcpe.encode("4221")).roles("USER");
 auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder());
 }
	
 @Override
 protected void configure(HttpSecurity http) throws Exception {
 http.formLogin();
 //http.authorizeRequests().antMatchers("/index").hasRole("USER");
 
 http.authorizeRequests().antMatchers("/save","/delete","/edit","/formMonument").hasRole("ADMIN");
 
 }
 @Bean
 BCryptPasswordEncoder getBCPE() {
	 return new BCryptPasswordEncoder();
 }
 
 
 }
