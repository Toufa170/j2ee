package monuPro.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;




@Entity
@Table(name = "Monument")
public class Monument implements Serializable {
	
	@Id @GeneratedValue
	private long id;
	// annotation de validation
	@NotNull
	@Size(min=5, max=6)
	private String codeM;
	private String typeMonument;
	private String nomM;
	private String proprietaire;
	private float longitude;
	private float latitude;
	
	@ManyToOne
	 @JoinColumn(name="FK_CodeInsee")
	private Lieu localite;
	
	@ManyToMany
	@JoinTable(name="associeA", joinColumns = @JoinColumn(name = "codeM"), inverseJoinColumns = @JoinColumn(name="numCelebrite"))
	private List<Celebrite> celebrites;
	
	public Monument() {
		super();
	}
	
	public Monument(long id, String codeM, String typeMonument, String nomM, String proprietaire, float longitude, float latitude) {
		super();
		this.id=id;
		this.codeM = codeM;
		this.typeMonument = typeMonument;
		this.nomM = nomM;
		this.proprietaire = proprietaire;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
		
	public String getCodeM() {
		return codeM;
	}
	public void setCodeM(String codeM) {
		this.codeM = codeM;
	}
	public String getTypeMonument() {
		return typeMonument;
	}
	public void setTypeMonument(String typeMonument) {
		this.typeMonument = typeMonument;
	}
	public String getNomM() {
		return nomM;
	}
	public void setNomM(String nomM) {
		this.nomM = nomM;
	}
	public String getProprietaire() {
		return proprietaire;
	}
	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

}
