package monuPro.entities;

	import java.io.Serializable;
	import java.util.List;

	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.ManyToMany;
	import javax.persistence.Table;
	import javax.persistence.JoinColumn;
	import javax.persistence.JoinTable;


	@Entity
	@Table(name = "celebrite")
	public class Celebrite implements Serializable {
			private static final long serialVersionUID = -5589677593780118174L;

			@Id
			@GeneratedValue
			private Long numCelebrite;

			

			private String nom;
			private String prenom;
			private String nationalite;
			private String epoque;
			
			@ManyToMany
			@JoinTable(name="associeA", joinColumns = @JoinColumn(name = "numCelebrite"), inverseJoinColumns = @JoinColumn(name="codeM"))
			private List<Monument> monuments;
			public Celebrite() {
				super();
				// TODO Auto-generated constructor stub
			}
			public Celebrite(Long numCelebrite, String nom, String prenom, String nationalite, String epoque) {
				super();
				this.numCelebrite = numCelebrite;
				this.nom = nom;
				this.prenom = prenom;
				this.nationalite = nationalite;
				this.epoque = epoque;
			}
			
			
			public Long getNumCelebrite() {
				return numCelebrite;
			}
			public void setNumCelebrite(Long numCelebrite) {
				this.numCelebrite = numCelebrite;
			}
			public String getNom() {
				return nom;
			}
			public void setNom(String nom) {
				this.nom = nom;
			}
			public String getPrenom() {
				return prenom;
			}
			public void setPrenom(String prenom) {
				this.prenom = prenom;
			}
			public String getNationalite() {
				return nationalite;
			}
			public void setNationalite(String nationalite) {
				this.nationalite = nationalite;
			}
			public String getEpoque() {
				return epoque;
			}
			public void setEpoque(String epoque) {
				this.epoque = epoque;
			}
			public static long getSerialversionuid() {
				return serialVersionUID;
			}

			

	

}
