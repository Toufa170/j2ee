package monuPro.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "departement")
public class Departement implements Serializable {
	private static final long serialVersionUID = 4183413417237191079L;

	@Id
	private long id;
	
    private String codeD;
	private String nom;
	private String reg;
	
	@OneToMany(mappedBy="dep",fetch=FetchType.LAZY)
	private Collection<Lieu> lieux;

	
	public Departement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Departement(long id, String codeD, String nom, String reg) {
		super();
		this.id=id;
		this.codeD = codeD;
		this.nom = nom;
		this.reg = reg;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return codeD;
	}

	public void setCode(String codeD) {
		this.codeD = codeD;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getReg() {
		return reg;
	}

	public void setReg(String reg) {
		this.reg = reg;
	}
    
	
}