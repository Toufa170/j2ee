package monuPro.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "lieu")
public class Lieu implements Serializable {
		private static final long serialVersionUID = 2942953051760927849L;

		@Id
		@Column(length = 10)
		private long id;
		
		private String codeInsee;
		private String nomCom;
		private float longitude;
		private float latitude;
		

		@OneToMany(mappedBy="localite",fetch=FetchType.LAZY)
		private List<Monument> monuments;
		
		 @ManyToOne
		 @JoinColumn(name="codeD")
		private Departement dep;
		
		public Lieu() {
			super();
			// TODO Auto-generated constructor stub
		}


		public Lieu(long id, String codeInsee,String nomCom, float longitude, float latitude) {
			super();
			this.id=id;
			this.codeInsee=codeInsee;
			this.nomCom = nomCom;
			this.longitude = longitude;
			this.latitude = latitude;
		}

		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		
		public String getCodeInsee() {
			return codeInsee;
		}


		public void setCodeInsee(String codeInsee) {
			this.codeInsee = codeInsee;
		}


		public String getNomCom() {
			return nomCom;
		}


		public void setNomCom(String nomCom) {
			this.nomCom = nomCom;
		}


		public float getLongitude() {
			return longitude;
		}


		public void setLongitude(float longitude) {
			this.longitude = longitude;
		}


		public float getLatitude() {
			return latitude;
		}


		public void setLatitude(float latitude) {
			this.latitude = latitude;
		}


		public static long getSerialversionuid() {
			return serialVersionUID;
		}


		public List getMonuments() {
			// TODO Auto-generated method stub
			return null;
		}
		
		

}