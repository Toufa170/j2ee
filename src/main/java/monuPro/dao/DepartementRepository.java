package monuPro.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import monuPro.entities.Departement;

public interface DepartementRepository extends JpaRepository<Departement, String> {

}
