package monuPro.dao;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import monuPro.entities.Monument;


public interface MonumentRepository extends JpaRepository<Monument, Long> {
	 
	public Page<Monument> findByNomMContains(String mot, Pageable pageable);
	
	//public Page<Monument> deleteById(long id, Pageable pageable);


	

	

}
