package monuPro.dao;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


import monuPro.entities.Celebrite;
import monuPro.entities.Monument;

public interface CelebriteRepository extends JpaRepository<Celebrite, Long>{

	

	public Page<Celebrite> findByNomContains(String mot, Pageable pageable);

		
}
