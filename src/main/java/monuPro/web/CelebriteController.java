package monuPro.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import monuPro.dao.CelebriteRepository;

import monuPro.entities.Celebrite;



@Controller
public class CelebriteController {
	
	@Autowired
	private CelebriteRepository celebriteRepository;
	@GetMapping(value="/index2")
	public String Chercher(Model model, 
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="motCle", defaultValue="") String mot){
		Page<Celebrite> pageCelebrites=
				celebriteRepository.findByNomContains(mot,PageRequest.of(page, 8));
		model.addAttribute("listCelebrites", pageCelebrites.getContent());
		model.addAttribute("pages", new int[pageCelebrites.getTotalPages()]);
		model.addAttribute("currentPage", page);
        model.addAttribute("motCle", mot);
		return "Celebrite";
	}
	
	@GetMapping("/delete2")
	public String Supprimer(Long numCelebrite) {
		celebriteRepository.deleteById(numCelebrite);
		return "redirect:/index2";
	}
	
	@GetMapping("/FormCelebrite")
	public String form (Model model){
		model.addAttribute("celebrite", new Celebrite());
		return "FormCelebrite";
		// validation avec spring securite
	}
	
	@GetMapping("/editCelebrite")
	public String edit(Model model, Long id){
		Celebrite celebrite=celebriteRepository.findById(id).get();
		model.addAttribute("celebrite", celebrite);
		return "EditCelebrite";
		
	}
	@PostMapping("/saveCelebrite")
	public String save(Model model, @Valid Celebrite celebrite, BindingResult bindingResult){
		if(bindingResult.hasErrors()) return "FormCelebrite";
		// signal l'erreur et retourne au formulaire
		celebriteRepository.save(celebrite);
		return "redirect:/index2";
	}
	
	

}
