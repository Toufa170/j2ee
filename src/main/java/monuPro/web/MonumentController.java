package monuPro.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import monuPro.dao.MonumentRepository;
import monuPro.entities.Monument;

@Controller
public class MonumentController {
	
	@Autowired
	private MonumentRepository monumentRepository;
	@GetMapping(value="/index")
	public String Chercher(Model model, 
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="motCle", defaultValue="") String mot){
		Page<Monument> pageMonuments=
				monumentRepository.findByNomMContains(mot,PageRequest.of(page, 8));
		model.addAttribute("listMonuments", pageMonuments.getContent());
		model.addAttribute("pages", new int[pageMonuments.getTotalPages()]);
		model.addAttribute("currentPage", page);
        model.addAttribute("motCle", mot);
		return "Monuments";
	}
	
	@GetMapping("/delete")
	public String Supprimer(Long id) {
		monumentRepository.deleteById(id);
		return "redirect:/index";
	}
	
	@GetMapping("/formMonument")
	public String form (Model model){
		model.addAttribute("monument", new Monument());
		return "FormMonument";
		// validation avec spring securite
	}
	
	@GetMapping("/edit")
	public String edit(Model model, Long id){
		Monument monument=monumentRepository.findById(id).get();
		model.addAttribute("monument", monument);
		return "EditMonument";
		
	}
	@PostMapping("/save")
	public String save(Model model, @Valid Monument monument, BindingResult bindingResult){
		if(bindingResult.hasErrors()) return "FormMonument";
		// signal l'erreur et retourne au formulaire
		monumentRepository.save(monument);
		return "redirect:/index";
	}
	
	
    }
