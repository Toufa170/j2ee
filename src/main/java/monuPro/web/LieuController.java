package monuPro.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import monuPro.dao.LieuRepository;
import monuPro.dao.MonumentRepository;
import monuPro.entities.Celebrite;
import monuPro.entities.Lieu;
import monuPro.entities.Monument;

@Controller
public class LieuController {
	@Autowired
	private LieuRepository lieuRepository;
	@GetMapping(value="/indexLieu")
	public String Chercher(Model model, 
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(name="motCle",defaultValue="") String mot){
		Page<Lieu> pageLieux=
				lieuRepository.findByCodeInseeContains(mot,PageRequest.of(page, 8));
		model.addAttribute("listLieux", pageLieux.getContent());
		model.addAttribute("pages", new int[pageLieux.getTotalPages()]);
		model.addAttribute("currentPage", page);
        model.addAttribute("motCle", mot);
		return "Lieu";
		
		
	
	}
	@GetMapping("/editLieu")
	public String edit(Model model, Long id){
		Lieu lieu=lieuRepository.findById(id).get();
		model.addAttribute("lieu", lieu);
		return "EditLieu";
	}
	
	@GetMapping("/FormLieu")
	public String form (Model model){
		model.addAttribute("lieu", new Lieu());
		return "FormLieu";
		// validation avec spring securite
	}
	
	
	@PostMapping("/saveLieu")
	public String save(Model model, @Valid Lieu lieu, BindingResult bindingResult){
		if(bindingResult.hasErrors()) return "FormLieu";
		// signal l'erreur et retourne au formulaire
		lieuRepository.save(lieu);
		return "redirect:/indexLieu";
	}
	
	@GetMapping("/deleteLieu")
	public String SupprimerLieu(Long id, int page, String motCle) {
		lieuRepository.deleteById(id);
		return "redirect:/indexLieu";
	}
	
	
	
	
	
	
	
}
