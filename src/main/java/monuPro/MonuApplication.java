package monuPro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import monuPro.dao.CelebriteRepository;
import monuPro.dao.DepartementRepository;
import monuPro.dao.LieuRepository;
//import monuPro.dao.CelebriteRepository;
import monuPro.dao.MonumentRepository;
import monuPro.entities.Celebrite;
import monuPro.entities.Departement;
import monuPro.entities.Lieu;
import monuPro.entities.Monument;

@SpringBootApplication
public class MonuApplication implements CommandLineRunner{
@Autowired
private MonumentRepository monumentRepository;
@Autowired
private CelebriteRepository celebriteRepository;

@Autowired
private LieuRepository lieuRepository;
@Autowired
private DepartementRepository departementRepository;



	public static void main(String[] args) {
		
	SpringApplication.run(MonuApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
	
		monumentRepository.save(new Monument(1, "34000","Place historique", "Place de la Comédie", "Mairie de Montpellier", 1.0f, 8.0f));
		monumentRepository.save(new Monument(2, "75000", "Place historique", "Arc de Triomphe", "Mairie de Paris", 3.5f, 5.0f));
		monumentRepository.save(new Monument(3, "13000", "Place historique", "porte d'Aix.", "Mairie de Marseille", 9.0f, 16.0f));
		monumentRepository.save(new Monument(4, "33000", "Place historique", "Place Pey Berland", "Mairie de Bordeaux", 12.56f, 6.0f));
		monumentRepository.findAll().forEach(m->{System.out.println(m.getNomM());
	});
		celebriteRepository.save(new Celebrite(12L,"Jean","CLaude", "Belge", "2000"));
		celebriteRepository.save(new Celebrite(13L, "Gaillard","Remi", "Française", "1980"));
		celebriteRepository.save(new Celebrite(14L, "Molière","Jean Baptiste", "Française", "1600"));
		celebriteRepository.save(new Celebrite(15L, "Soprano","Saïd", "Française", "2015"));
		
		celebriteRepository.findAll().forEach(c->{System.out.println(c.getNom());
		});
		
		lieuRepository.save(new Lieu(16L,"34172", "Montpellier" , 2.0f, 2.0f));
		lieuRepository.save(new Lieu(17L, "75012" ,"Paris", 3.0f, 4.1f));
		lieuRepository.save(new Lieu(18L, "13055" ,"Marseille", 3.77f, 4.1f));
		lieuRepository.save(new Lieu(19L, "33063" ,"Bordeaux", 3.21f, 4.6f));
		
		lieuRepository.findAll().forEach(l->{System.out.println(l.getCodeInsee());
		});
		

		departementRepository.save(new Departement(20L, "34000", "Montpellier", "hhd"));
		departementRepository.save(new Departement(21L, "34000", "Paris", "hgd"));
		departementRepository.save(new Departement(22L, "13000", "Marseille", "lgf"));
		departementRepository.save(new Departement(23L, "34000", "Bordeaux", "sry"));
		
		departementRepository.findAll().forEach(d->{System.out.println(d.getNom());
		});
		
		
	
		
	}

	


	
}
